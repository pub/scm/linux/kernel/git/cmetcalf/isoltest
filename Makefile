LDFLAGS += -pthread
CFLAGS ?= -O2 -g
CFLAGS += -W -Wall

isolation: isolation.c

clean:
	rm -f isolation
